/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>
#include <time.h>
#include <errno.h>

using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment

void *Task(void *arg);

typedef long data_t;
const uint V_FUSION = 256;
const uint MAX_COLOR = 255;
const uint MIN_COLOR = 0;
const unsigned NUM_THREADS = 8;
const char* SOURCE_IMG = "bailarina.bmp";
const char* SOURCE_IMG2 = "background_V.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

void* Task(void* arg);

// Open file and object initialization
CImg<data_t> srcImage(SOURCE_IMG);
CImg<data_t> srcImage2(SOURCE_IMG2);

data_t* pRsrc, * pGsrc, * pBsrc; // Pointers to the R, G and B components
data_t* pRdest, * pGdest, * pBdest;

data_t* pRsrc2, * pGsrc2, * pBsrc2; //Punteros de la segunda imagen 
data_t* pRdest2, * pGdest2, * pBdest2;

data_t* pDstImage; // Pointer to the new image pixels
data_t* pDstImage2;

uint width, height; // Width and height of the image
uint nComp; // Number of image components

float Rt = 0.7196;
float Gt = 0.1725;
float Bt = 0.1176;

int main() {
	//Calculo componentes color sepia
	//Color RGB se compone de un 21,96% de rojo, 17,25% de verde y 11,76% de azul


	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

	srcImage.display(); // Displays the source image
	width = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and

	// Allocate memory space for destination image components
	pDstImage = (data_t*)malloc(width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	pDstImage2 = (data_t*)malloc(width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;


	pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
	pGsrc2 = pRsrc2 + height * width; // pGcomp points to the G component array
	pBsrc2 = pGsrc2 + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest2 = pDstImage2;
	pGdest2 = pRdest2 + height * width;
	pBdest2 = pGdest2 + height * width;

	/*
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	printf("\nMeasuring time:\n");
	fflush(stdout);
	if (clock_gettime(CLOCK_REALTIME, &tStart) == -1) {
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	pthread_t threads[NUM_THREADS];

	for (uint n_rep = 0; n_rep < 5; n_rep++) {
		for (uint i = 0; i < NUM_THREADS; i++) {
			void* pointer = &i;
			int pthread_ret = pthread_create(&threads[i], NULL, Task, pointer);
			if (pthread_ret != 0) {
				printf("ERROR: pthread_create error code: %d.\n", pthread_ret);
				exit(EXIT_FAILURE);
			}
			//fflush(stdout);
			pthread_join(threads[i], NULL);
		}
	}



	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if (clock_gettime(CLOCK_REALTIME, &tEnd) == -1) {
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

	printf("\nTime elapsed: %f\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.

	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG);

	// Display destination image
	dstImage.display();
	CImg<data_t> dst2Image(pDstImage2, width, height, 1, nComp);
	dst2Image.save(DESTINATION_IMG);
	dst2Image.display();
	// Free memory
	free(pDstImage);
	free(pDstImage2);
	return 0;
}
void *Task(void* arg) {

	//currentPArt es el trozo de matriz que el hilo esta procesando
	//nextPart es donde empieza el siguiente hilo
	int tamParte = height / NUM_THREADS;
	int parteActual = tamParte * (*((int*) arg));
	int sigParte = parteActual + tamParte;
    // Realizamos el tintado
	uint t;
	
	for (uint i = parteActual; i < sigParte; i++){
		for (uint k = 0; k < width; k++) {
			t=i * width + k;
		*(pRdest + t) = *(pRsrc + t) + (MAX_COLOR - *(pRsrc + t)) * Rt;
		*(pGdest + t) = *(pGsrc + t) + (MAX_COLOR - *(pGsrc + t)) * Gt;
		*(pBdest + t) = *(pBsrc + t) + (MAX_COLOR - *(pBsrc + t)) * Bt;


		if (*(pRdest + t) > MAX_COLOR) { *(pRdest + t) = MAX_COLOR; }
		if (*(pGdest + t) > MAX_COLOR) { *(pGdest + t) = MAX_COLOR; }
		if (*(pBdest + t) > MAX_COLOR) { *(pBdest + t) = MAX_COLOR; }

		if (*(pRdest + t) < MIN_COLOR) { *(pRdest + t) = MIN_COLOR; }
		if (*(pGdest + t) < MIN_COLOR) { *(pGdest + t) = MIN_COLOR; }
		if (*(pBdest + t) < MIN_COLOR) { *(pBdest + t) = MIN_COLOR; }
			
		}
	}
	//Realizamos la fusión
	for (uint i = parteActual; i < sigParte; i++){
		for (uint k = 0; k < width; k++) {
			t=i * width + k;
				*(pRdest2 + t)=( *(pRsrc2 + t)* V_FUSION)/( *(pRsrc + t)+1);
				*(pGdest2 + t)=( *(pGsrc2 + t)* V_FUSION)/( *(pGsrc + t)+1);
				*(pBdest2 + t)=( *(pBsrc2 + t)* V_FUSION)/( *(pBsrc + t)+1);
				
				if (*(pRdest2 + t) > MAX_COLOR){ *(pRdest2 + t) = MAX_COLOR; }
				if (*(pGdest2 + t) > MAX_COLOR){ *(pGdest2 + t) = MAX_COLOR;}
				if (*(pBdest2 + t) > MAX_COLOR){ *(pBdest2 + t) = MAX_COLOR;}

				if (*(pRdest + t) < MIN_COLOR){ *(pRdest + t) = MIN_COLOR;}
				if (*(pGdest + t) < MIN_COLOR){ *(pGdest + t) = MIN_COLOR;}
				if (*(pBdest + t) < MIN_COLOR){ *(pBdest + t) = MIN_COLOR;}
					
		}
		
	}
	
pthread_exit(0);

}